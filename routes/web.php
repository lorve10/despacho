<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdminController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/login');
});
// Route::get('/encrypted', [AdminController::class, 'encrypted']);

Auth::routes();

Route::group(['middleware' => 'auth'],function(){
  Route::get('/Menu', [AdminController::class, 'menu']);
  Route::get('/Conductor', [AdminController::class, 'Conductor']);
  Route::post('/saveConductor', [AdminController::class, 'saveConductor']);
  Route::get('/listConductor', [AdminController::class, 'list']);
  Route::post('/deleteConductor', [AdminController::class, 'deleteConductor']);
  Route::get('/logout', [AdminController::class, 'logout_admin'])->name('admin.logout');

//rutas vehiculos
Route::get('/Vehiculo', [AdminController::class, 'Vehiculo']);
Route::post('/saveVehiculo', [AdminController::class, 'saveVehiculo']);
Route::get('/listVe', [AdminController::class, 'listVe']);
Route::post('/deleteVehiculo', [AdminController::class, 'deleteVehiculo']);
//rutas Despacho
Route::get('/Despacho', [AdminController::class, 'Despacho']);
Route::post('/saveDespacho', [AdminController::class, 'saveDespacho']);
Route::get('/listDespacho', [AdminController::class, 'listDespacho']);
Route::post('/deleteDespacho', [AdminController::class, 'deleteDespacho']);
//ruta usuario
Route::get('/Usuario', [AdminController::class, 'Usuario']);
Route::post('/saveUser', [AdminController::class, 'saveUser']);
Route::get('/listUser', [AdminController::class, 'listUser']);
Route::get('/perfil', [AdminController::class, 'perfil']);
Route::get('/Perfil', [AdminController::class, 'datos']);

});
