<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Despacho extends Model
{
    use HasFactory;
    protected $table='despacho';

    protected $fillable = [
        'id',
        'conductor',
        'vehiculo',
        'hora',
    ];
    public function persona(){
       return $this->HasOne("App\Models\Conductor","id","conductor")->where("deleted", 0);
   }
   public function prueba(){
      return $this->HasOne("App\Models\Vehiculo","id","vehiculo")->where("deleted", 0);
  }
}
