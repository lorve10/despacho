<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Vehiculo extends Model
{
    use HasFactory;
    protected $table='vehiculo';

    protected $fillable = [
        'id',
        'placa',
        'numero',
        'conductor',
        'deleted',
    ];

    public function persona(){
       return $this->HasOne("App\Models\Conductor","id","conductor")->where("deleted", 0);
   }
}
