<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Conductor;
use App\Models\Vehiculo;
use App\Models\Despacho;
use App\Models\User;
use Log;
use Hash;

class AdminController extends Controller
{
    public function menu(){
      return view('Admin.Welcome');
    }
    public function Conductor(){
      return view('Admin.Conductor');
    }

    public function logout_admin()
    {
     auth()->logout();
     return redirect('/');
    }

    public function encrypted()
    {
     $pass = "12345678";
     $encript = Hash::make($pass);
     echo $encript;
    }

    public function saveConductor (Request $request){
      try {

        $id = $request['id'];
        $base64_image = $request['firma'];
        // $base64_str = substr($base64_image, strpos($base64_image, ",")+1);
        // $image = base64_decode($base64_str);
        // Log::info($image);
        // file_put_contents('imagen.png', $datos);


        $folderPath = public_path().'/storage/imagen/png';

        $image_parts = explode(";base64,", $base64_image);

        $image_type_aux = explode("image/", $image_parts[0]);

        $image_type = $image_type_aux[1];

        $image_base64 = base64_decode($image_parts[1]);

        $file = $folderPath . uniqid() . '.'.$image_type;
         file_put_contents($file, $image_base64);
         $hola = array("C:\Users\Usuario\Desktop\Despachos\public");
         $remplaze = str_replace($hola,"", $file);
         Log::info($remplaze);


        $data['nombre'] = $request['nombre'];
        $data['apellido'] = $request['apellido'];
        $data['identificacion'] = $request['identificacion'];
        $data['firma'] = $remplaze;

        if($id > 0){
          Conductor::find($id)->update($data);
        }
        else{
          Conductor::create($data);
        }
        return response()->json([ 'message' => "Successfully created", "success" => true ], 200);
      } catch (\Exception $e) {
        return response()->json([ 'message' => $e->getMessage(), "success" => false ], 500);
      }
    }

    ///Función para listar
  public function list(){
    try {
      $data = Conductor::where('deleted',0)->get();
      return response()->json(['message' => "Successfully loaded", 'data' => $data, "success" => true], 200);
    } catch (\Exception $e) {
      return response()->json(['message' => $e->getMessage(), 'success' => false ], 500);
    }
  }

  public function deleteConductor(Request $request){
  try {
      $id = $request['id'];
        Conductor::where('id', $request['id'])->update([
      'deleted'=> 1
    ]);
      return response()->json([ 'message' => "Successfully created", "success" => true ], 200);

  } catch (\Exception $e){
    return response()->json([ 'message' => $e->getMessage(), "success" => false], 500);
  }
}


  public function Vehiculo(){
    return view('Admin.Vehiculo');
  }

  public function saveVehiculo(Request $request){
    try {

      $id = $request['id'];

      $data['placa'] = $request['placa'];
      $data['numero'] = $request['numero'];
      $data['conductor'] = $request['conductor'];

      if($id > 0){
        Vehiculo::find($id)->update($data);
      }
      else{
        Vehiculo::create($data);
      }
      return response()->json([ 'message' => "Successfully created", "success" => true ], 200);
    } catch (\Exception $e) {
      return response()->json([ 'message' => $e->getMessage(), "success" => false ], 500);
    }
  }

  public function listVe(){
    try {
      $data = Vehiculo::with("persona")->where('deleted',0)->get();
      return response()->json(['message' => "Successfully loaded", 'data' => $data, "success" => true], 200);
    } catch (\Exception $e) {
      return response()->json(['message' => $e->getMessage(), 'success' => false ], 500);
    }
  }


  public function deleteVehiculo(Request $request){
  try {
        $id = $request['id'];
        Vehiculo::where('id', $id)->update([
      'deleted'=> 1
    ]);
      return response()->json([ 'message' => "Successfully created", "success" => true ], 200);

  } catch (\Exception $e){
    return response()->json([ 'message' => $e->getMessage(), "success" => false], 500);
  }
}

  public function Despacho(){
    return view('Admin.Despacho');
  }

  public function saveDespacho(Request $request){
    try {

      $id = $request['id'];

      $data['conductor'] = $request['conductor'];
      $data['vehiculo'] = $request['vehiculo'];
      $data['hora'] = $request['hora'];

      if($id > 0){
        Despacho::find($id)->update($data);
      }
      else{
        Despacho::create($data);
      }
      return response()->json([ 'message' => "Successfully created", "success" => true ], 200);
    } catch (\Exception $e) {
      return response()->json([ 'message' => $e->getMessage(), "success" => false ], 500);
    }
  }

  public function listDespacho(){
    try {
      $data = Despacho::with("persona","prueba")->where('deleted',0)->get();
      return response()->json(['message' => "Successfully loaded", 'data' => $data, "success" => true], 200);
    } catch (\Exception $e) {
      return response()->json(['message' => $e->getMessage(), 'success' => false ], 500);
    }
  }

  public function deleteDespacho(Request $request){
  try {
        $id = $request['id'];
        Despacho::where('id', $id)->update([
      'deleted'=> 1
    ]);
      return response()->json([ 'message' => "Successfully created", "success" => true ], 200);

  } catch (\Exception $e){
    return response()->json([ 'message' => $e->getMessage(), "success" => false], 500);
  }
}
//// usuariosss
  public function Usuario(){
    return view('Admin.Usuario');
  }
  public function saveUser(Request $request){
    try {
      $id = $request['id'];
      $base64_image = $request['firma'];

      $folderPath = public_path().'/storage/imagen/png';

      $image_parts = explode(";base64,", $base64_image);

      $image_type_aux = explode("image/", $image_parts[0]);

      $image_type = $image_type_aux[1];

      $image_base64 = base64_decode($image_parts[1]);

      $file = $folderPath . uniqid() . '.'.$image_type;
       file_put_contents($file, $image_base64);
       $hola = array("C:\Users\Usuario\Desktop\Despachos\public");
       $remplaze = str_replace($hola,"", $file);
       Log::info($remplaze);

      $data['name'] = $request['name'];
      $data['email'] = $request['email'];
      $data['nombre'] = $request['nombre'];
      $data['apellido'] = $request['apellido'];
      $data['password'] = Hash::make($request['password']);
      $data['firma'] = $remplaze;



      if($id > 0){
        User::find($id)->update($data);
      }
      else{
        User::create($data);
      }
      return response()->json([ 'message' => "Successfully created", "success" => true ], 200);
    } catch (\Exception $e) {
      return response()->json([ 'message' => $e->getMessage(), "success" => false ], 500);
    }
  }

  public function listUser(){
    try {
      $data = User::get();
      return response()->json(['message' => "Successfully loaded", 'data' => $data, "success" => true], 200);
    } catch (\Exception $e) {
      return response()->json(['message' => $e->getMessage(), 'success' => false ], 500);
    }
  }

public function perfil()
{
  try {
    $loguet = Auth()->user()->id;
    $data = User::where('id', $loguet)->get();
    return response()->json(['message' => "Successfully loaded", 'data' => $data, "success" => true], 200);
  } catch (\Exception $e) {
    return response()->json(['message' => $e->getMessage(), 'success' => false ], 500);
  }

}
//// usuariosss
  public function datos(){
    return view('Admin.Perfil');
  }


}
