import react , { useState, useEffect, useRef} from 'react';
import Popup from 'reactjs-popup';
import SignaturePad from 'react-signature-canvas';
import '../../../../css/app.css';
import api from '../../servi/api';
import Swal from 'sweetalert2';
const ip = 'http://localhost:8000';


const Form  = ({ goback, list, id }) =>{

  const [ nombre, setNombre ] = useState('');
  const [ apellido, setApellido ] = useState('');
  const [ identificacion, setIdentificacion ] = useState('');
  const [ firma, setFirma ] = useState(null);
  const [ edit, setEdit] = useState(false);
  const [ loadingSave, setLoadingSave ] = useState(false);

  const sigCanva = useRef({})
  const limpiar = () => sigCanva.current.clear();

  const MessageError = async (data) => {
      Swal.fire({
        title: 'Error',
        text: data,
        icon: 'warning',
      })
    }
    const MessageSuccess = async (data) => {
      Swal.fire({
        text: data,
        icon: 'success',
      })
    }

    useEffect(()=>{
        validar()
    },[])

    const validar = () =>{
      if (id > 0) {
          setEdit(true)
          setNombre(list.nombre)
          setApellido(list.apellido)
          setIdentificacion(list.identificacion)
          setFirma(ip+list.firma)
      }
    }


  const save = () =>{
    setFirma(sigCanva.current.getTrimmedCanvas().toDataURL("image/png"));

    let message = ''
    let error = false

    console.log(firma);
    if (nombre == "") {
      error = true
      message = "Debes agregar tu nombre"
    }
    else if (apellido == "") {
      error = true
      message = "Debes agregar un apellido"
    }
    if (error) {
      MessageError(message)
    }else {
      const data = new FormData();
      data.append('id', id);
      data.append('nombre', nombre);
      data.append('apellido', apellido);
      data.append('identificacion', identificacion);
      data.append('firma', firma);
      api.post('saveConductor', data).then(data=>{
        if (data.success) {
          if(id == null){
            MessageSuccess("Conductor registrado exitosamente")
            goback();
          }
          else{
            MessageSuccess("Editado Correctamente")
            goback();
          }
        }
      }).catch(err => console.warn(err))
    }
  }

  const changed = () =>{
    setEdit(false);
    setFirma(null)

  }

  return(
    <div style = {{width:'100%'}}>
      <div  className="form-group">
        <div className="row">
          <div className="col-md-6">
            <label htmlFor="Identificación" className = "titulo-form">Identificación</label>
            <input value = {identificacion} className="form-control" placeholder = "Identificación del conductor" maxLength = "70" onChange = {(e)=>setIdentificacion(e.target.value)} id = "price" type="number" name="price"/>
        </div>
          <div className="col-md-6">
            <label htmlFor="Nombre" className = "titulo-form">Nombre conductor</label>
            <input value = {nombre} className="form-control" placeholder = "Nombre del condutor" maxLength = "70" onChange = {(e)=>setNombre(e.target.value)} id = "Nombre" type="text" name="nombre"/>
          </div>
          <div className="col-md-6">
            <label htmlFor="Nombre" className = "titulo-form">Apellido</label>
            <input value = {apellido} className="form-control" placeholder = "Apellido del conductor" maxLength = "70" onChange = {(e)=>setApellido(e.target.value)} id = "price" type="text" name="price"/>
          </div>

          {
            edit == true ?
            <div className="col-md-5 mt-1" style={{border:'1px solid #000000',borderRadius:12, marginLeft:12}}>
              <img style = {{ maxWidth: 300, maxHeight: 300 }} src={firma} />
                <button style={{marginLeft:101}} className="btn btn-primary" onClick = {()=>changed()}>Cambiar firma</button>

            </div>
            :
            <div className="col-md-5 mt-1"  style={{border:'1px solid #000000',borderRadius:12, marginLeft:12}}>
              <label htmlFor="Firma" className = "titulo-form">Firma</label>
              <SignaturePad
                ref = {sigCanva}
                canvasProps={{
                  className:"propiedades"
                }}
                 />
               <a style={{color:'blue', cursor:'pointer'}} onClick={()=>limpiar()} >limpiar</a>
            </div>
          }

        </div>
        {
          !loadingSave ?
          <div className="my-2 d-flex col-12 justify-content-end px-0" >
            <button className="btn btn-warning mr-2" onClick = {()=>goback()} >Regresar</button>
            <button className="btn btn-success" onClick = {()=>save()}>Guardar Cambios</button>
          </div>
          :<div className="my-2 d-flex col-md-5 justify-content-end px-0" style = {{float:'right'}}>
          <button v-if="id" className="btn btn-success">
            <div className="spinner-grow text-light" role="status">
              <span className="sr-only">Loading...</span>
            </div>

          </button>
        </div>
      }
    </div>
    </div>
  )
}
export default Form;
