import React, { useState, useEffect  }  from 'react';
import Swal from 'sweetalert2'
import api from '../../servi/api';
const ip = 'http://localhost:8000';
import util from '../../means/util';
import Pagination from '../../means/paginate';



function List ({ dataUpdate }) {
  const [ text, setText ] = useState('')
  const [ list, setList ] = useState([])
  const [ showForm, setShowForm ] = useState(1)

  const [ listContenido, setListContenido ] = useState([])
  /// variables paginador
  const [ listCustBack, setListCustBack ] = useState([])
  const [ currentPage, setCurrentPage ] = useState(1)
  const [ perPage, setPerPage ] = useState(5)


  useEffect(()=>{
    obtain_Con();
  },[])

  const obtain_Con = () =>{
    api.get('listVe')
    .then(data =>{
      setList(data)
      setListContenido(data)
      setListCustBack(data)
    }).catch(console.warn);
}



///funcion para filtrar datos de clientes en la tabla
const filterOrderText = (event) => {
  var text = event.target.value
  const data = listContenido
  const newData = data.filter(function(item){
      var textAll = " "
      textAll = textAll+" "+item.numero
      textAll = textAll+" "+item.placa.toUpperCase()

      const textData = text.toUpperCase()
      return textAll.indexOf(textData) > -1
  })
  setList(newData)
  setText(text)
}

const updateCurrentPage = async (number) => {
    await setList([])
    await setCurrentPage(number)
    const dataNew = util.paginate(listCustBack,number,perPage)
    await setList(dataNew)
  }

  const deshabilitar = async (value) => {
    var message = '¿Está seguro que quiere eliminar este vehiculo?'
    Swal.fire({
      title: message,
      showDenyButton: true,
      confirmButtonText: `Sí`,
      denyButtonText: `No`,
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        const data = new FormData()
        data.append('id', value)
        api.post('deleteVehiculo',data).then(response=>{
          obtain_Con();
          Swal.fire('Eliminado correctamente!', '', 'success')
        })

      } else if (result.isDenied) {
        Swal.fire('Acción cancelada', '', 'info')
      }
    })
  }

  const editar = (data) =>{
    dataUpdate(data)
  }


  return (
    <div>

        <div className="content-wrapper" style={{minHeight:600, marginLeft:0}}>
          <div >
            <div className="">
              <div className="col-12 mt-5 mb-3 d-flex justify-content-between align-items-center" style={{height: 40, border:'1px solid #e4e4e4', borderRadius:20, backgroundColor:'white'}}>
                  <input onChange = {(e)=>filterOrderText(e)} style = {{width:'inherit',border:'none', fontSize: 14, outlineStyle:'auto', outlineWidth:0}} value = {text} placeholder="Digite el numero y placa"/>
              </div>
            </div>
            <div className = "card-body">
         <div>
            <table className="table table-striped">
              <thead>
                <tr>
                    <th>Placa</th>
                    <th>Numero interno</th>
                    <th>Conductor</th>
                    <th>Acciones</th>
                </tr>
                {
                  list.map((item)=>{
                    return (
                      <tr>
                        <td>{item.placa}</td>
                        <td>{item.numero}</td>
                        <td>{item.persona.identificacion+"-"+item.persona.nombre+"-"+item.persona.apellido }</td>
                        <td>
                          <div className = "d-flex">
                            <button onClick = {()=>deshabilitar(item.id)} className="btn btn-danger"><i className= "material-icons">delete</i></button>
                          </div>
                        </td>
                        <td>
                          <button onClick = {()=>editar(item)} className="btn btn-danger"><i className= "material-icons">edit</i></button>
                        </td>
                      </tr>
                    );
                  })

                }

              </thead>
              <tbody>
              </tbody>
            </table>
            <div className="d-flex col-md-12 col-12 justify-content-end mt-2">
              <Pagination currentPage={currentPage} perPage={perPage} countdata={listCustBack.length} updateCurrentPage={(number)=>updateCurrentPage(number)}/>
            </div>
            </div>
        </div>
    </div>
  </div>
</div>
);
}

export default List;
