import react , { useState, useEffect, useRef} from 'react';
import Popup from 'reactjs-popup';
import '../../../../css/app.css';
import api from '../../servi/api';
import Swal from 'sweetalert2';
import Select from 'react-select';
import makeAnimated from 'react-select/animated';



const Form  = ({ goback, list, id }) =>{
  const [ condutor, setConductor ] = useState(null)
  const [ conduSelect, setConduSelect ] = useState([])
  const [ placa, setPlaca ] = useState('');
  const [ numeroInterno, setNumeroInterno ] = useState('');
  const [ loadingSave, setLoadingSave ] = useState(false);

  const [animatedComponents, setAnimatedComponents] = useState(makeAnimated);

  useEffect(()=>{
      obtain_Con();
  },[])

  const obtain_Con = () =>{
    api.get('listConductor')
    .then(data =>{
        let res = data;
        let con = []
        res.map(item=>{
          const data = {
              value: item.id,
              label: item.identificacion+" - "+item.nombre +" "+ item.apellido
          }
          con.push(data);
          setConduSelect(con)
        })

        if(id > 0){
          let filtro = res.filter(e=>e.id == list.conductor);
          const data = {
            value: filtro[0].id,
            label: filtro[0].identificacion+" - "+filtro[0].nombre +" "+ filtro[0].apellido
           }
           setConductor(data);
        }
    }).catch(console.warn);
}


  const MessageError = async (data) => {
      Swal.fire({
        title: 'Error',
        text: data,
        icon: 'warning',
      })
    }
    const MessageSuccess = async (data) => {
      Swal.fire({
        text: data,
        icon: 'success',
      })
    }

    useEffect(()=>{
        validar()
    },[])

    const validar = () =>{
      if (id > 0) {
          setPlaca(list.placa)
          setNumeroInterno(list.numero)
      }
    }


  const save = () =>{

    let message = ''
    let error = false

    if (placa == "") {
      error = true
      message = "Debes agregar una placa"
    }
    else if (numeroInterno == "") {
      error = true
      message = "Debes agregar un numero interno"
    }
    if (error) {
      MessageError(message)
    }else {
      const data = new FormData();
      data.append('id', id);
      data.append('placa', placa);
      data.append('numero', numeroInterno);
      data.append('conductor', condutor.value);
      api.post('saveVehiculo', data).then(data=>{
        if (data.success) {
          if(id == null){
            MessageSuccess("Conductor registrado exitosamente")
            goback();
          }
          else{
            MessageSuccess("Editado Correctamente")
            goback();
          }
        }
      }).catch(err => console.warn(err))
    }
  }

  const changed = () =>{
    setEdit(false);
    setFirma(null)

  }

  return(
    <div style = {{width:'100%'}}>
      <div  className="form-group">
        <div className="row">
          <div className="col-md-6">
            <label htmlFor="placa" className = "titulo-form">Placa</label>
            <input value = {placa} className="form-control" placeholder = "Numero de la placa" maxLength = "70" onChange = {(e)=>setPlaca(e.target.value)} id = "price" type="text" name="price"/>
        </div>
          <div className="col-md-6">
            <label htmlFor="Nombre" className = "titulo-form">numero interno</label>
            <input value = {numeroInterno} className="form-control" placeholder = "Numero interno" maxLength = "70" onChange = {(e)=>setNumeroInterno(e.target.value)} id = "Nombre" type="number" name="nombre"/>
          </div>
          <div className="col-md-6">
            <label htmlFor="Nombre" className = "titulo-form">Conductor</label>
            <Select
              value={condutor}
              closeMenuOnSelect={true}
              components={animatedComponents}
              options={conduSelect}
              onChange={(e)=>setConductor(e)}
              placeholder = "Seleccionar conductor"
              name="colors"
              />
          </div>



        </div>
        {
          !loadingSave ?
          <div className="my-2 d-flex col-12 justify-content-end px-0" >
            <button className="btn btn-warning mr-2" onClick = {()=>goback()} >Regresar</button>
            <button className="btn btn-success" onClick = {()=>save()}>Guardar Cambios</button>
          </div>
          :<div className="my-2 d-flex col-md-5 justify-content-end px-0" style = {{float:'right'}}>
          <button v-if="id" className="btn btn-success">
            <div className="spinner-grow text-light" role="status">
              <span className="sr-only">Loading...</span>
            </div>

          </button>
        </div>
      }
    </div>
    </div>
  )
}
export default Form;
