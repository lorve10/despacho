import react , { useState, useEffect, useRef} from 'react';
import ReactDOM from 'react-dom';
import Popup from 'reactjs-popup';
import '../../../../css/app.css';
import api from '../../servi/api';
import Swal from 'sweetalert2';
import SignaturePad from 'react-signature-canvas';
const ip = 'http://localhost:8000';


const InfoUser  = () =>{
  const [ id, setId ] = useState(null)
  const [ nombre, setNombre ] = useState('')
  const [ apellido, setApellido ] = useState('')
  const [ user, setUser ] = useState('')
  const [ pass, setPass ] = useState('')
  const [ firma, setFirma ] = useState('')
  const [ edit, setEdit] = useState(false);
  const [ email, setEmail ] = useState('')
  const [ loadingSave, setLoadingSave ] = useState(false);

  const sigCanva = useRef({})
  const limpiar = () => sigCanva.current.clear();

  useEffect(()=>{
    api.get('perfil')
    .then(data =>{
      let res = data
      res.map(item=>{
        setId(item.id)
        setUser(item.name)
        setNombre(item.nombre)
        setApellido(item.apellido)
        setFirma(ip+item.firma)
        setEmail(item.email)
      })
      setEdit(true)
    }).catch(console.warn);
  },[])


  const MessageError = async (data) => {
      Swal.fire({
        title: 'Error',
        text: data,
        icon: 'warning',
      })
    }
    const MessageSuccess = async (data) => {
      Swal.fire({
        text: data,
        icon: 'success',
      })
    }




  const save = () =>{
    setFirma(sigCanva.current.getTrimmedCanvas().toDataURL("image/png"));

    let message = ''
    let error = false

    if (nombre == null) {
      error = true
      message = "Debes agregar un vehiculo"
    }
    else if (email ==null) {
      error = true
      message = "Debes agregar un conductor"
    }
    if (error) {
      MessageError(message)
    }else {
      const data = new FormData();
      data.append('id', id);
      data.append('name', user);
      data.append('nombre', nombre);
      data.append('apellido', apellido);
      data.append('email', email);
      data.append('firma', firma);
      data.append('password', pass);
      api.post('saveUser', data).then(data=>{
        if (data.success) {
          if(id == null){
            MessageSuccess("Conductor registrado exitosamente")
            goback();
          }
          else{
            MessageSuccess("Editado Correctamente")
            goback();
          }
        }
      }).catch(err => console.warn(err))
    }
  }

  const changed = () =>{
    setEdit(false);
    setFirma(null)

  }

  return(
<div className="content-wrapper mt-5 m-5" style={{minHeight:600, marginLeft:0}}>
<div className="card" style={{borderRadius:10}}>
  <div className="card-header">
      <i className="nav-icon fas fa-edit mr-2" >Datos de mi perfil</i>
  </div>
    <div style = {{width:'100%'}}>
      <div  className="form-group">
        <div className="row">

          <div className="col-md-6">
            <label htmlFor="Nombre" className = "titulo-form">Nombre</label>
            <input value = {nombre} className="form-control" placeholder = "Nombre" maxLength = "70" onChange = {(e)=>setNombre(e.target.value)} id = "price" type="text" name="price"/>
         </div>
          <div className="col-md-6">
            <label htmlFor="apellido" className = "titulo-form">Apellido</label>
            <input value = {apellido} className="form-control" placeholder = "Apellido" maxLength = "70" onChange = {(e)=>setApellido(e.target.value)} id = "price" type="text" name="price"/>
        </div>
        <div className="col-md-6">
          <label htmlFor="usuarios" className = "titulo-form">Usuario</label>
          <input value = {user} className="form-control" placeholder = "Usuario" maxLength = "70" onChange = {(e)=>setUser(e.target.value)} id = "price" type="text" name="price"/>
        </div>
        <div className="col-md-6">
          <label htmlFor="email" className = "titulo-form">Email</label>
          <input value = {email} className="form-control" placeholder = "Email" maxLength = "70" onChange = {(e)=>setEmail(e.target.value)} id = "price" type="text" name="price"/>
        </div>
        <div className="col-md-6">
          <label htmlFor="contraseña" className = "titulo-form">contraseña</label>
          <input value = {pass} className="form-control" placeholder = "Usuario" maxLength = "70" onChange = {(e)=>setPass(e.target.value)} id = "price" type="password" name="price"/>
        </div>
        {
          edit == true ?
          <div className="mt-2 col-md-5 mt-1" style={{border:'1px solid #000000',borderRadius:12, marginLeft:12}}>
            <img style = {{ maxWidth: 300, maxHeight: 300 }} src={firma} />
              <button style={{marginLeft:101}} className="btn btn-primary" onClick = {()=>changed()}>Cambiar firma</button>

          </div>
          :
          <div className="mt-2 col-md-5 justify-content-center mt-1"  style={{border:'1px solid #000000',borderRadius:12, marginLeft:12}}>
            <label htmlFor="Firma" className = "titulo-form">Firma</label>
            <SignaturePad
              ref = {sigCanva}
              canvasProps={{
                className:"propiedades"
              }}
               />
             <a style={{color:'blue', cursor:'pointer'}} onClick={()=>limpiar()} >limpiar</a>
          </div>
        }


        </div>
        {
          !loadingSave ?
          <div className="my-2 d-flex col-12 justify-content-end px-0" >
            <button className="btn btn-success" onClick = {()=>save()}>Actualizar</button>
          </div>
          :<div className="my-2 d-flex col-md-5 justify-content-end px-0" style = {{float:'right'}}>
          <button v-if="id" className="btn btn-success">
            <div className="spinner-grow text-light" role="status">
              <span className="sr-only">Loading...</span>
            </div>

          </button>
        </div>
      }
    </div>

  </div>
  </div>
  </div>
  )
}
export default InfoUser;
if (document.getElementById('InfoUser')) {
  ReactDOM.render(<InfoUser />, document.getElementById('InfoUser'));
}
