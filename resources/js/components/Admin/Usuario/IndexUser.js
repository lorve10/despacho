import React, { useState, useEffect  }  from 'react';
import ReactDOM from 'react-dom';
import Form from './Form';
import List from './list';


function IndexUser() {
  const [showForm, setShowForm] = useState(1)
  const [cat, setCat] = useState([])
  const [id, setId] = useState(null)
  const [data, setData] = useState({})
  const [ estado, setEstado ] = useState(false)

  const goback = () => {
    setShowForm(1)
    setId(null)
    setEstado(false);
  }

  const dataUpdate = async (data) => {
    setEstado(true);
    let name = data.name
    let nombre = data.nombre
    let apellido = data.apellido
    let email = data.email
    let firma = data.firma

    const dataForAll = {
      name,
      nombre,
      apellido,
      email,
      firma
    }
    await setId(data.id)
    await setData(dataForAll)
    await setShowForm(2)
  }


  return (
    <div >

        <div className="content-wrapper mt-5" style={{minHeight:600, marginLeft:0}}>
          <div className="card" style={{borderRadius:10}}>
            <div className="card-header">
              <h4 className="form-section d-flex align-items-center">
              {
                  showForm == 1 ?
                  <i className="nav-icon fas fa-edit mr-2" >Lista de usuarios</i>
                  : showForm == 2 ?
                  <i className="nav-icon fas fa-edit mr-2" >
                    {
                      estado == false ?
                      "Registrar un Despacho"
                      : estado == true?
                      "Editar"
                      :null
                    }

                  </i>
                  :null

              }
            </h4>
            </div>
            <div className = "card-body">
              {
                showForm == 1 &&

                    <div className="row justify-content-end">
                      <button onClick = {()=>setShowForm(2)} className="col-md-1 btn btn-outline-success round btn-min-width mr-1 mb-1">Agregar</button>
                    </div>


              }
              <div className = "row padding-forms-admin">
                {

                  showForm == 2 ?
                  <Form    list={data}  goback = {()=>goback()}  id = {id} />
                  :showForm == 1 ?
                  <List  dataUpdate={(data)=>dataUpdate(data)} />
                  :null

                }
              </div>
            </div>
          </div>
        </div>

    </div>

);
}

export default IndexUser;

if (document.getElementById('IndexUser')) {
  ReactDOM.render(<IndexUser />, document.getElementById('IndexUser'));
}
