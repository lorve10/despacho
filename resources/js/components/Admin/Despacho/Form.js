import react , { useState, useEffect, useRef} from 'react';
import Popup from 'reactjs-popup';
import '../../../../css/app.css';
import api from '../../servi/api';
import Swal from 'sweetalert2';
import Select from 'react-select';
import makeAnimated from 'react-select/animated';



const Form  = ({ goback, list, id }) =>{

  const [ conductor, setConductor ] = useState(null)
  const [ vehiculo, setVehiculo ] = useState(null)
  const [ conduSelect, setConduSelect ] = useState([])
  const [ vehiculoSelect, setVehiculoSelect] = useState([])
  const [ placa, setPlaca ] = useState('');
  const [ hora, setHora ] = useState('');
  const [ loadingSave, setLoadingSave ] = useState(false);

  const [animatedComponents, setAnimatedComponents] = useState(makeAnimated);

  useEffect(()=>{
      obtain_Con();
      obtain_ve()
  },[])

  const obtain_Con = () =>{
    api.get('listConductor')
    .then(data =>{
        let res = data;
        let con = []
        res.map(item=>{
          const data = {
              value: item.id,
              label: item.identificacion+" - "+item.nombre +" "+ item.apellido
          }
          con.push(data);
          setConduSelect(con)
        })

        if(id > 0){
          let filtro = res.filter(e=>e.id == list.conductor);
          const data = {
            value: filtro[0].id,
            label: filtro[0].identificacion+" - "+filtro[0].nombre +" "+ filtro[0].apellido
           }
           setConductor(data);
           obtain_ve(filtro[0].id, false)

        }
    }).catch(console.warn);
}

const filtrado = (value) =>{
    setConductor(value);
    obtain_ve(value.value, true)
}

const obtain_ve = (value, boolean) =>{
  setVehiculoSelect([])
  api.get('listVe')
  .then(data =>{
      let res = data;
      let con = []
      console.log(value);
      let filtrado = res.filter(e =>e.conductor == value);
      console.log(filtrado);
      filtrado.map(item=>{
        const data = {
            value: item.id,
            label: item.numero+ "-" +item.placa
        }
        con.push(data);
        setVehiculoSelect(con)
      })
      if(id > 0){
        let filtro = res.filter(e=>e.id == list.vehiculo);
        const data = {
          value: filtro[0].id,
          label: filtro[0].numero+ "-" +filtro[0].placa
         }
         setVehiculo(data);
      }

  }).catch(console.warn);
}

  const MessageError = async (data) => {
      Swal.fire({
        title: 'Error',
        text: data,
        icon: 'warning',
      })
    }
    const MessageSuccess = async (data) => {
      Swal.fire({
        text: data,
        icon: 'success',
      })
    }

    useEffect(()=>{
      if (id > 0) {
          setHora(list.hora)
      }
    },[id])


  const save = () =>{

    let message = ''
    let error = false

    if (vehiculo == null) {
      error = true
      message = "Debes agregar un vehiculo"
    }
    else if (conductor ==null) {
      error = true
      message = "Debes agregar un conductor"
    }
    if (error) {
      MessageError(message)
    }else {
      const data = new FormData();
      data.append('id', id);
      data.append('vehiculo', vehiculo.value);
      data.append('conductor', conductor.value);
      data.append('hora', hora);
      api.post('saveDespacho', data).then(data=>{
        if (data.success) {
          if(id == null){
            MessageSuccess("Conductor registrado exitosamente")
            goback();
          }
          else{
            MessageSuccess("Editado Correctamente")
            goback();
          }
        }
      }).catch(err => console.warn(err))
    }
  }

  const changed = () =>{
    setEdit(false);
    setFirma(null)

  }

  return(
    <div style = {{width:'100%'}}>
      <div  className="form-group">
        <div className="row">

          <div className="col-md-6">
            <label htmlFor="Nombre" className = "titulo-form">Conductor</label>
            <Select
              value={conductor}
              closeMenuOnSelect={true}
              components={animatedComponents}
              options={conduSelect}
              onChange={(e)=>filtrado(e)}
              placeholder = "Seleccionar conductor"
              name="colors"
              />
          </div>
          <div className="col-md-6">
            <label htmlFor="Nombre" className = "titulo-form">Vehiculo</label>
            <Select
              value={vehiculo}
              closeMenuOnSelect={true}
              components={animatedComponents}
              options={vehiculoSelect}
              onChange={(e)=>setVehiculo(e)}
              placeholder = "Seleccionar vehiculo"
              name="colors"
              />
          </div>
          <div className="col-md-6">
            <label htmlFor="Nombre" className = "titulo-form">Hora despacho</label>
            <input value={hora} className="form-control" placeholder = "hora despacho" maxLength = "70" onChange = {(e)=>setHora(e.target.value)} id="hora" type="time"/>
          </div>


        </div>
        {
          !loadingSave ?
          <div className="my-2 d-flex col-12 justify-content-end px-0" >
            <button className="btn btn-warning mr-2" onClick = {()=>goback()} >Regresar</button>
            <button className="btn btn-success" onClick = {()=>save()}>Guardar Cambios</button>
          </div>
          :<div className="my-2 d-flex col-md-5 justify-content-end px-0" style = {{float:'right'}}>
          <button v-if="id" className="btn btn-success">
            <div className="spinner-grow text-light" role="status">
              <span className="sr-only">Loading...</span>
            </div>

          </button>
        </div>
      }
    </div>
    </div>
  )
}
export default Form;
