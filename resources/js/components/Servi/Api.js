import axios from 'axios';
const ip = 'http://localhost:8000/';

const http = {}


http.get = async (url) =>{
      const {data} = await axios.get(ip + url)
        .then(response=>{
            return response.data;
        }).catch(console.warn)
        return data;
}

http.post = async (url, data)=>{
    let res = await axios.post(ip+ url, data).then(response=>{
      return response.data;
  }).catch(console.warn)
  return res;
}






export default http;
